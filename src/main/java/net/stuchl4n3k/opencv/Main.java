package net.stuchl4n3k.opencv;

import java.awt.Desktop;
import java.io.File;
import java.net.URI;
import nu.pattern.OpenCV;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;

/**
 * @author petr.stuchlik
 */
public class Main {
    static {
        OpenCV.loadShared();
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) throws Exception {
        String inputFilePath = ClassLoader.getSystemResource("profile.jpg").getPath();

        CascadeClassifier faceDetector = new CascadeClassifier(ClassLoader.getSystemResource("haarcascade_frontalface_alt.xml").getPath());
        Mat image = Highgui.imread(inputFilePath);

        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);

        System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

        for (Rect rect : faceDetections.toArray()) {
            Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }

        String outputFilePath = new File("output.png").getAbsolutePath();

        System.out.println(String.format("Writing %s", outputFilePath));
        Highgui.imwrite(outputFilePath, image);

        Desktop.getDesktop().browse(new File(outputFilePath).toURI());
    }

}

